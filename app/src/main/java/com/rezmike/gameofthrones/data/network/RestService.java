package com.rezmike.gameofthrones.data.network;

import com.rezmike.gameofthrones.data.network.responces.CharacterInfoRes;
import com.rezmike.gameofthrones.data.network.responces.HouseInfoRes;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface RestService {

    @GET("houses/362")
    Call<HouseInfoRes> getStarkHouse();

    @GET("houses/229")
    Call<HouseInfoRes> getLannisterHouse();

    @GET("houses/378")
    Call<HouseInfoRes> getTargaryenHouse();

    @GET("characters?pageSize=50")
    Call<ArrayList<CharacterInfoRes>> getAllCharacters(@Query("page") int pageNumber);

    @GET
    Call<CharacterInfoRes> getCharacterByUrl(@Url String url);
}