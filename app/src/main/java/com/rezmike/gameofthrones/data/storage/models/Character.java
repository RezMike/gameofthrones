package com.rezmike.gameofthrones.data.storage.models;

import com.rezmike.gameofthrones.data.network.responces.CharacterInfoRes;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.JoinProperty;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.ToMany;
import org.greenrobot.greendao.annotation.Unique;

import java.util.List;

import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;

import static android.R.attr.name;

@Entity(active = true, nameInDb = "CHARACTERS")
public class Character {

    @Id
    private Long id;

    @NotNull
    @Unique
    private String remoteId;

    @NotNull
    private String name;

    private String words;

    private String born;

    private String died;

    private String father;

    private String mother;

    private String lastSeries;

    @ToMany(joinProperties = {
            @JoinProperty(name = "remoteId", referencedName = "userRemoteId")
    })
    private List<Title> titles;

    @ToMany(joinProperties = {
            @JoinProperty(name = "remoteId", referencedName = "userRemoteId")
    })
    private List<Alias> aliases;

    public Character(CharacterInfoRes character, String remoteId, String words) {
        this.remoteId = remoteId;
        name = character.getName();
        this.words = words;
        born = character.getBorn();
        died = character.getDied();
        String fatherUrl = character.getFather();
        father = fatherUrl.substring(fatherUrl.lastIndexOf("/") + 1, fatherUrl.length());
        String motherUrl = character.getMother();
        mother = motherUrl.substring(motherUrl.lastIndexOf("/") + 1, motherUrl.length());
        List<String> series = character.getTvSeries();
        if (!series.isEmpty()) {
            lastSeries = series.get(series.size()-1);
        } else lastSeries = "";
    }

    /**
     * Used for active entity operations.
     */
    @Generated(hash = 898307126)
    private transient CharacterDao myDao;

    /**
     * Used to resolve relations
     */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Resets a to-many relationship, making the next get call to query for a fresh result.
     */
    @Generated(hash = 731614754)
    public synchronized void resetAliases() {
        aliases = null;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 1291789899)
    public List<Alias> getAliases() {
        if (aliases == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            AliasDao targetDao = daoSession.getAliasDao();
            List<Alias> aliasesNew = targetDao._queryCharacter_Aliases(remoteId);
            synchronized (this) {
                if (aliases == null) {
                    aliases = aliasesNew;
                }
            }
        }
        return aliases;
    }

    /**
     * Resets a to-many relationship, making the next get call to query for a fresh result.
     */
    @Generated(hash = 1506933621)
    public synchronized void resetTitles() {
        titles = null;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 1657258360)
    public List<Title> getTitles() {
        if (titles == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            TitleDao targetDao = daoSession.getTitleDao();
            List<Title> titlesNew = targetDao._queryCharacter_Titles(remoteId);
            synchronized (this) {
                if (titles == null) {
                    titles = titlesNew;
                }
            }
        }
        return titles;
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 162219484)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getCharacterDao() : null;
    }

    public String getMother() {
        return this.mother;
    }

    public void setMother(String mother) {
        this.mother = mother;
    }

    public String getFather() {
        return this.father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    public String getBorn() {
        return this.born;
    }

    public void setBorn(String born) {
        this.born = born;
    }

    public String getWords() {
        return this.words;
    }

    public void setWords(String words) {
        this.words = words;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemoteId() {
        return this.remoteId;
    }

    public void setRemoteId(String remoteId) {
        this.remoteId = remoteId;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastSeries() {
        return this.lastSeries;
    }

    public void setLastSeries(String lastSeries) {
        this.lastSeries = lastSeries;
    }

    public String getDied() {
        return this.died;
    }

    public void setDied(String died) {
        this.died = died;
    }

    @Generated(hash = 2032501212)
    public Character(Long id, @NotNull String remoteId, @NotNull String name, String words,
            String born, String died, String father, String mother, String lastSeries) {
        this.id = id;
        this.remoteId = remoteId;
        this.name = name;
        this.words = words;
        this.born = born;
        this.died = died;
        this.father = father;
        this.mother = mother;
        this.lastSeries = lastSeries;
    }

    @Generated(hash = 1853959157)
    public Character() {
    }
}
