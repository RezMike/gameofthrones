package com.rezmike.gameofthrones.data.managers;

import android.content.Context;

import com.rezmike.gameofthrones.GOTApplication;
import com.rezmike.gameofthrones.data.network.RestService;
import com.rezmike.gameofthrones.data.network.ServiceGenerator;
import com.rezmike.gameofthrones.data.network.responces.CharacterInfoRes;
import com.rezmike.gameofthrones.data.network.responces.HouseInfoRes;
import com.rezmike.gameofthrones.data.storage.models.DaoSession;

import org.greenrobot.greendao.async.AsyncSession;

import java.util.ArrayList;

import retrofit2.Call;

public class DataManager {
    private static DataManager INSTANCE = null;

    private Context mContext;
    private DaoSession mDaoSession;
    private RestService mRestService;

    private DataManager() {
        mContext = GOTApplication.getContext();
        mDaoSession = GOTApplication.getDaoSession();
        mRestService = ServiceGenerator.createService(RestService.class);
    }

    public static DataManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DataManager();
        }
        return INSTANCE;
    }

    public Context getContext() {
        return mContext;
    }

    //region ==================== Network =============================

    public Call<HouseInfoRes> getStarksHouse() {
        return mRestService.getStarkHouse();
    }

    public Call<HouseInfoRes> getLannisterHouse() {
        return mRestService.getLannisterHouse();
    }

    public Call<HouseInfoRes> getTargaryenHouse() {
        return mRestService.getTargaryenHouse();
    }

    public Call<ArrayList<CharacterInfoRes>> getAllCharacters(int pageNumber) {
        return mRestService.getAllCharacters(pageNumber);
    }

    public Call<CharacterInfoRes> getCharacterByUrl(String url) {
        return mRestService.getCharacterByUrl(url);
    }

    //endregion

    //region ==================== Database =======================

    public DaoSession getDaoSession() {
        return mDaoSession;
    }

    public AsyncSession getAsyncSession() {
        return mDaoSession.startAsyncSession();
    }

    //endregion
}