package com.rezmike.gameofthrones.utils;

public class StringUtils {
    public static String getIdFromUrl(String url) {
        return url.substring(url.lastIndexOf("/") + 1, url.length());
    }
}
