package com.rezmike.gameofthrones.utils;

public interface ConstantManager {
    String EXTRA_CHARACTER_ID = "EXTRA_CHARACTER_ID";
    String HOUSE_ID = "HOUSE_ID";

    String STARKS_WORDS = "Winter is Coming";
    String LANNISTERS_WORDS = "Hear Me Roar!";
    String TANGARYENS_WORDS = "Fire and Blood";

    int STARKS_HOUSE_ID = 101;
    int LANNISTERS_HOUSE_ID = 102;
    int TARGARYENS_HOUSE_ID = 103;
}
