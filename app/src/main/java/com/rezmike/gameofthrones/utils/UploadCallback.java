package com.rezmike.gameofthrones.utils;

public interface UploadCallback {
    void onUploaded();
}
