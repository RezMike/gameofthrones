package com.rezmike.gameofthrones;

import android.app.Application;
import android.content.Context;

import com.facebook.stetho.Stetho;
import com.rezmike.gameofthrones.data.storage.models.DaoMaster;
import com.rezmike.gameofthrones.data.storage.models.DaoSession;

import org.greenrobot.greendao.database.Database;

public class GOTApplication extends Application {

    private static Context sContext;
    private static DaoSession sDaoSession;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = this;

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "devintensive-db");
        Database db = helper.getWritableDb();
        sDaoSession = new DaoMaster(db).newSession();

        Stetho.initializeWithDefaults(this);
    }

    public static Context getContext() {
        return sContext;
    }

    public static DaoSession getDaoSession() {
        return sDaoSession;
    }
}
