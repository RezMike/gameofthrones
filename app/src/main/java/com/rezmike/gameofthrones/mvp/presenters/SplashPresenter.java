package com.rezmike.gameofthrones.mvp.presenters;

import com.rezmike.gameofthrones.R;
import com.rezmike.gameofthrones.mvp.models.SplashModel;
import com.rezmike.gameofthrones.mvp.views.ISplashView;
import com.rezmike.gameofthrones.utils.UploadCallback;

public class SplashPresenter implements ISplashPresenter, UploadCallback {

    private static SplashPresenter ourInstance = new SplashPresenter();

    private SplashModel mSplashModel;
    private ISplashView mSplashView;

    private SplashPresenter() {
        mSplashModel = new SplashModel();
    }

    public static SplashPresenter getInstance() {
        return ourInstance;
    }

    @Override
    public void takeView(ISplashView view) {
        mSplashView = view;
    }

    @Override
    public void dropView() {
        mSplashView = null;
    }

    @Override
    public void initView() {
        if (getView() == null) return;
        if (mSplashModel.checkNetwork()) {
            getView().showLoad();
            if (mSplashModel.isDbEmpty()) {
                mSplashModel.loadData(this);
            } else {
                onUploaded();
            }
        } else {
            getView().showMessage(R.string.network_not_available);
        }
    }

    @Override
    public ISplashView getView() {
        return mSplashView;
    }

    @Override
    public void onUploaded() {
        mSplashView.onDataUploaded();
    }
}