package com.rezmike.gameofthrones.mvp.presenters;

import com.rezmike.gameofthrones.mvp.views.ISplashView;

public interface ISplashPresenter {
    void takeView(ISplashView view);
    void dropView();
    void initView();
    ISplashView getView();
}
