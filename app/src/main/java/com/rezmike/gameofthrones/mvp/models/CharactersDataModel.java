package com.rezmike.gameofthrones.mvp.models;

import com.rezmike.gameofthrones.data.managers.DataManager;
import com.rezmike.gameofthrones.data.storage.models.Character;
import com.rezmike.gameofthrones.data.storage.models.CharacterDao;
import com.rezmike.gameofthrones.data.storage.models.DaoSession;
import com.rezmike.gameofthrones.utils.ConstantManager;

import java.util.List;

public class CharactersDataModel {
    private DataManager mDataManager;
    private DaoSession mDaoSession;

    public CharactersDataModel() {
        mDataManager = DataManager.getInstance();
        mDaoSession = mDataManager.getDaoSession();
    }

    public List<Character> getStarkList() {
        return mDaoSession.queryBuilder(Character.class)
                .where(CharacterDao.Properties.Words.eq(ConstantManager.STARKS_WORDS))
                .orderAsc(CharacterDao.Properties.Name)
                .build()
                .list();
    }

    public List<Character> getLannisterList() {
        return mDaoSession.queryBuilder(Character.class)
                .where(CharacterDao.Properties.Words.eq(ConstantManager.LANNISTERS_WORDS))
                .orderAsc(CharacterDao.Properties.Name)
                .build()
                .list();
    }

    public List<Character> getTangaryenList() {
        return mDaoSession.queryBuilder(Character.class)
                .where(CharacterDao.Properties.Words.eq(ConstantManager.TANGARYENS_WORDS))
                .orderAsc(CharacterDao.Properties.Name)
                .build()
                .list();
    }

    public Character getCharacter(long characterId) {
        return mDaoSession.queryBuilder(Character.class)
                .where(CharacterDao.Properties.RemoteId.eq(characterId)).unique();
    }
}
