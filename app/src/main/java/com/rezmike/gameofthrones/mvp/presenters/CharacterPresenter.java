package com.rezmike.gameofthrones.mvp.presenters;

import com.rezmike.gameofthrones.data.storage.models.Character;
import com.rezmike.gameofthrones.mvp.models.CharactersDataModel;
import com.rezmike.gameofthrones.mvp.views.ICharacterView;

public class CharacterPresenter implements ICharacterPresenter {

    private static CharacterPresenter ourInstance = new CharacterPresenter();

    private CharactersDataModel mModel;
    private ICharacterView mView;

    public static CharacterPresenter getInstance() {
        return ourInstance;
    }

    private CharacterPresenter() {
        mModel = new CharactersDataModel();
    }

    @Override
    public void takeView(ICharacterView view) {
        mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
    }

    @Override
    public void initView() {
        mView.initCharacter();
    }

    @Override
    public ICharacterView getView() {
        return mView;
    }

    @Override
    public Character getCharacter(long characterId) {
        if (characterId != 0) {
            return mModel.getCharacter(characterId);
        }
        return null;
    }
}
