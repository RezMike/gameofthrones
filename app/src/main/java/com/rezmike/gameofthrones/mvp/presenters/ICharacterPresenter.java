package com.rezmike.gameofthrones.mvp.presenters;

import com.rezmike.gameofthrones.data.storage.models.Character;
import com.rezmike.gameofthrones.mvp.views.ICharacterView;

public interface ICharacterPresenter {
    void takeView(ICharacterView view);
    void dropView();
    void initView();

    ICharacterView getView();

    Character getCharacter(long characterId);
}
