package com.rezmike.gameofthrones.mvp.presenters;

import com.rezmike.gameofthrones.data.storage.models.Character;
import com.rezmike.gameofthrones.mvp.views.IHouseListView;

import java.util.List;

public interface IHouseListPresenter {
    void takeView(IHouseListView view);
    void dropView();
    void initView();

    IHouseListView getView();

    void onItemClick(long remoteId);

    List<Character> getStarkList();
    List<Character> getLannisterList();
    List<Character> getTangaryenList();
}
