package com.rezmike.gameofthrones.mvp.views;

import com.rezmike.gameofthrones.mvp.presenters.IActivityListPresenter;

public interface IActivityListView {

    void showMessage(String message);
    void showMessage(int stringResourceId);
    void showError(Throwable e);

    IActivityListPresenter getPresenter();

    void showStarkHouse();
    void showLannisterHouse();
    void showTangaryenHouse();
}
