package com.rezmike.gameofthrones.mvp.views;

import com.rezmike.gameofthrones.mvp.presenters.IHouseListPresenter;

public interface IHouseListView {

    void showMessage(String message);
    void showMessage(int stringResourceId);
    void showError(Throwable e);

    IHouseListPresenter getPresenter();

    void initializeCharacters();
    void onItemClick(long remoteId);
}
