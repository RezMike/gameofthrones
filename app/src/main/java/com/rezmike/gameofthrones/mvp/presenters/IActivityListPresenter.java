package com.rezmike.gameofthrones.mvp.presenters;

import com.rezmike.gameofthrones.mvp.views.IActivityListView;

public interface IActivityListPresenter {

    void takeView(IActivityListView view);
    void dropView();
    void initView();

    IActivityListView getView();

    void showStarkHouse();
    void showLannisterHouse();
    void showTangaryenHouse();
}
