package com.rezmike.gameofthrones.mvp.models;

import com.rezmike.gameofthrones.data.managers.DataManager;
import com.rezmike.gameofthrones.data.network.responces.CharacterInfoRes;
import com.rezmike.gameofthrones.data.network.responces.HouseInfoRes;
import com.rezmike.gameofthrones.data.storage.models.Alias;
import com.rezmike.gameofthrones.data.storage.models.Character;
import com.rezmike.gameofthrones.data.storage.models.Title;
import com.rezmike.gameofthrones.utils.ConstantManager;
import com.rezmike.gameofthrones.utils.NetworkStatusChecker;
import com.rezmike.gameofthrones.utils.StringUtils;
import com.rezmike.gameofthrones.utils.UploadCallback;

import org.greenrobot.greendao.async.AsyncSession;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashModel {
    private DataManager mDataManager;
    private UploadCallback mUploadCallback;

    private List<CharacterInfoRes> mCharactersInfoResList;
    private List<String> mCharactersWordsList;
    private List<Character> mCharactersList;
    private List<Title> mTitleList;
    private List<Alias> mAliasList;
    private int mUploadedCharactersCount;
    private int mCurrentCharactersCount;

    public SplashModel() {
        mDataManager = DataManager.getInstance();
    }

    public boolean isDbEmpty() {
        return mDataManager.getDaoSession().queryBuilder(Character.class).build().list().size() == 0;
    }

    public void loadData(UploadCallback callback) {
        mUploadCallback = callback;
        uploadHouses();
    }

    public boolean checkNetwork() {
        return NetworkStatusChecker.isNetworkAvailable(mDataManager.getContext());
    }

    //region ======================== Upload data ========================

    private void uploadHouses() {
        mUploadedCharactersCount = 0;
        mCurrentCharactersCount = 0;
        mCharactersInfoResList = new ArrayList<>();
        mCharactersWordsList = new ArrayList<>();
        Call<HouseInfoRes> starksHouseCall = mDataManager.getStarksHouse();
        starksHouseCall.enqueue(new Callback<HouseInfoRes>() {
            @Override
            public void onResponse(Call<HouseInfoRes> call, Response<HouseInfoRes> response) {
                if (response.code() == 200) {
                    mUploadedCharactersCount += response.body().getSwornMembers().size();
                    for (String url : response.body().getSwornMembers()) {
                        uploadCharacterByUrl(url, ConstantManager.STARKS_WORDS);
                    }
                }
            }

            @Override
            public void onFailure(Call<HouseInfoRes> call, Throwable t) {

            }
        });

        Call<HouseInfoRes> lannisterHouseCall = mDataManager.getLannisterHouse();
        lannisterHouseCall.enqueue(new Callback<HouseInfoRes>() {
            @Override
            public void onResponse(Call<HouseInfoRes> call, Response<HouseInfoRes> response) {
                if (response.code() == 200) {
                    mUploadedCharactersCount += response.body().getSwornMembers().size();
                    for (String url : response.body().getSwornMembers()) {
                        uploadCharacterByUrl(url, ConstantManager.LANNISTERS_WORDS);
                    }
                }
            }

            @Override
            public void onFailure(Call<HouseInfoRes> call, Throwable t) {

            }
        });

        Call<HouseInfoRes> targaryenHouseCall = mDataManager.getTargaryenHouse();
        targaryenHouseCall.enqueue(new Callback<HouseInfoRes>() {
            @Override
            public void onResponse(Call<HouseInfoRes> call, Response<HouseInfoRes> response) {
                if (response.code() == 200) {
                    mUploadedCharactersCount += response.body().getSwornMembers().size();
                    for (String url : response.body().getSwornMembers()) {
                        uploadCharacterByUrl(url, ConstantManager.TANGARYENS_WORDS);
                    }
                }
            }

            @Override
            public void onFailure(Call<HouseInfoRes> call, Throwable t) {

            }
        });
    }

    private void uploadCharacterByUrl(String url, final String words) {
        Call<CharacterInfoRes> call = mDataManager.getCharacterByUrl(url);
        call.enqueue(new Callback<CharacterInfoRes>() {
            @Override
            public void onResponse(Call<CharacterInfoRes> call, Response<CharacterInfoRes> response) {
                if (response.code() == 200) {
                    mCharactersInfoResList.add(response.body());
                    mCharactersWordsList.add(words);
                }
                mCurrentCharactersCount++;
                checkCharactersLoaded();
            }

            @Override
            public void onFailure(Call<CharacterInfoRes> call, Throwable t) {

            }
        });
    }

    private void checkCharactersLoaded() {
        if (mCurrentCharactersCount == mUploadedCharactersCount) {
            saveCharactersInDb();
        }
    }

    //endregion

    //region ======================== Save data ========================

    private void addCharacterToList(CharacterInfoRes character, String words) {
        String characterId = StringUtils.getIdFromUrl(character.getUrl());
        mCharactersList.add(new Character(character, characterId, words));

        for (String title : character.getTitles()) {
            mTitleList.add(new Title(characterId, title));
        }

        for (String alias : character.getAliases()) {
            mAliasList.add(new Alias(characterId, alias));
        }
    }

    private void saveCharactersInDb() {
        mCharactersList = new ArrayList<>();
        mTitleList = new ArrayList<>();
        mAliasList = new ArrayList<>();
        for (int i = 0; i < mCharactersInfoResList.size(); i++) {
            addCharacterToList(mCharactersInfoResList.get(i), mCharactersWordsList.get(i));
        }

        AsyncSession asyncSession = mDataManager.getAsyncSession();
        asyncSession.insertOrReplaceInTx(Character.class, mCharactersList);
        asyncSession.insertOrReplaceInTx(Title.class, mTitleList);
        asyncSession.insertOrReplaceInTx(Alias.class, mAliasList);

        mCharactersInfoResList = null;
        mCharactersList = null;
        mTitleList = null;
        mAliasList = null;
        mUploadCallback.onUploaded();
    }

    //endregion
}
