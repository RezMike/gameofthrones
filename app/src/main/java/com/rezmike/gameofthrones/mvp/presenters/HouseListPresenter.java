package com.rezmike.gameofthrones.mvp.presenters;

import com.rezmike.gameofthrones.data.storage.models.Character;
import com.rezmike.gameofthrones.mvp.models.CharactersDataModel;
import com.rezmike.gameofthrones.mvp.views.IHouseListView;

import java.util.List;

public class HouseListPresenter implements IHouseListPresenter {

    private static HouseListPresenter ourInstance = new HouseListPresenter();

    private CharactersDataModel mModel;
    private IHouseListView mView;

    private List<Character> mStarkList;
    private List<Character> mLannisterList;
    private List<Character> mTangaryenList;

    public static HouseListPresenter getInstance() {
        return ourInstance;
    }

    private HouseListPresenter() {
        mModel = new CharactersDataModel();
    }

    @Override
    public void takeView(IHouseListView view) {
        mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
    }

    @Override
    public void initView() {
        mView.initializeCharacters();
    }

    @Override
    public IHouseListView getView() {
        return mView;
    }

    @Override
    public void onItemClick(long remoteId) {
        mView.onItemClick(remoteId);
    }

    @Override
    public List<Character> getStarkList() {
        if (mStarkList == null) {
            mStarkList = mModel.getStarkList();
        }
        return mStarkList;
    }

    @Override
    public List<Character> getLannisterList() {
        if (mLannisterList == null) {
            mLannisterList = mModel.getLannisterList();
        }
        return mLannisterList;
    }

    @Override
    public List<Character> getTangaryenList() {
        if (mTangaryenList == null) {
            mTangaryenList = mModel.getTangaryenList();
        }
        return mTangaryenList;
    }
}
