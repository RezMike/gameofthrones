package com.rezmike.gameofthrones.mvp.presenters;

import com.rezmike.gameofthrones.mvp.views.IActivityListView;

public class ActivityListPresenter implements IActivityListPresenter {
    private static ActivityListPresenter ourInstance = new ActivityListPresenter();

    private IActivityListView mView;

    public static ActivityListPresenter getInstance() {
        return ourInstance;
    }

    @Override
    public void takeView(IActivityListView view) {
        mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
    }

    @Override
    public void initView() {

    }

    @Override
    public IActivityListView getView() {
        return mView;
    }

    @Override
    public void showStarkHouse() {
        mView.showStarkHouse();
    }

    @Override
    public void showLannisterHouse() {
        mView.showLannisterHouse();
    }

    @Override
    public void showTangaryenHouse() {
        mView.showTangaryenHouse();
    }
}
