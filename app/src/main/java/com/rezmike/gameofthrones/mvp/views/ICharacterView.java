package com.rezmike.gameofthrones.mvp.views;

import com.rezmike.gameofthrones.mvp.presenters.ICharacterPresenter;

public interface ICharacterView {

    void showMessage(String message);
    void showMessage(int stringResourceId);
    void showError(Throwable e);

    ICharacterPresenter getPresenter();

    void initCharacter();
}
