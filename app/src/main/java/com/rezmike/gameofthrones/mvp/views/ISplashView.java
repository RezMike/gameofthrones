package com.rezmike.gameofthrones.mvp.views;

import com.rezmike.gameofthrones.mvp.presenters.ISplashPresenter;

public interface ISplashView {

    void showMessage(String message);
    void showMessage(int stringResourceId);
    void showError(Throwable e);

    void showLoad();
    void hideLoad();

    ISplashPresenter getPresenter();

    void onDataUploaded();
}
