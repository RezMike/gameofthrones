package com.rezmike.gameofthrones.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.rezmike.gameofthrones.BuildConfig;
import com.rezmike.gameofthrones.R;
import com.rezmike.gameofthrones.mvp.presenters.ISplashPresenter;
import com.rezmike.gameofthrones.mvp.presenters.SplashPresenter;
import com.rezmike.gameofthrones.mvp.views.ISplashView;


public class SplashActivity extends AppCompatActivity implements ISplashView {
    SplashPresenter mPresenter = SplashPresenter.getInstance();
    ProgressDialog mProgressDialog;

    private boolean isAllSaved = false;
    private boolean isTimeOut = false;

    //region ======================== Activity cycle ========================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mPresenter.takeView(this);
        mPresenter.initView();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                isTimeOut = true;
                nextActivity();
            }
        }, 1000);
    }

    @Override
    protected void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }

    //endregion

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showMessage(int stringResourceId) {
        showMessage(getString(stringResourceId));
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage(getString(R.string.sorry_something_wrong));
            // TODO: 23.10.2016 send error stacktrace to crashlytics
        }
    }

    @Override
    public void showLoad() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this, R.style.custom_dialog);
            mProgressDialog.setCancelable(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        mProgressDialog.show();
        mProgressDialog.setContentView(R.layout.progress_splash);
    }

    @Override
    public void hideLoad() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public ISplashPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void onDataUploaded() {
        isAllSaved = true;
        nextActivity();
    }

    private void nextActivity() {
        if (isAllSaved && isTimeOut) {
            hideLoad();
            Intent intent = new Intent(SplashActivity.this, CharacterListActivity.class);
            startActivity(intent);
            SplashActivity.this.finish();
        }
    }
}