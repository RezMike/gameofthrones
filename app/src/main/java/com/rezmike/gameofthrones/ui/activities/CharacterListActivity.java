package com.rezmike.gameofthrones.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.rezmike.gameofthrones.BuildConfig;
import com.rezmike.gameofthrones.R;
import com.rezmike.gameofthrones.mvp.presenters.ActivityListPresenter;
import com.rezmike.gameofthrones.mvp.presenters.IActivityListPresenter;
import com.rezmike.gameofthrones.mvp.views.IActivityListView;
import com.rezmike.gameofthrones.ui.fragments.HouseListFragment;
import com.rezmike.gameofthrones.ui.views.CircleImageView;
import com.rezmike.gameofthrones.utils.ConstantManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CharacterListActivity extends AppCompatActivity
        implements HouseListFragment.HouseListCallbacks, IActivityListView {

    ActivityListPresenter mPresenter = ActivityListPresenter.getInstance();

    @BindView(R.id.coordinator_layout)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.navigation_view)
    NavigationView mNavigationView;
    @BindView(R.id.navigation_drawer)
    DrawerLayout mNavigationDrawer;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tablayout)
    TabLayout mTabLayout;
    @BindView(R.id.viewpager)
    ViewPager mViewPager;

    //region ======================== Activity cycle ========================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character_list);
        ButterKnife.bind(this);

        mPresenter.takeView(this);
        mPresenter.initView();

        setupToolbar();
        setupViewPager();
        setupDrawer();
    }

    @Override
    protected void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }

    //endregion

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            mNavigationDrawer.openDrawer(GravityCompat.START);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mNavigationDrawer.isDrawerOpen(GravityCompat.START)) {
            mNavigationDrawer.closeDrawer(GravityCompat.START);
        } else {
            moveTaskToBack(true);
        }
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void setupViewPager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(HouseListFragment.newInstance(ConstantManager.STARKS_HOUSE_ID), getString(R.string.tab_starks));
        adapter.addFragment(HouseListFragment.newInstance(ConstantManager.LANNISTERS_HOUSE_ID), getString(R.string.tab_lannisters));
        adapter.addFragment(HouseListFragment.newInstance(ConstantManager.TARGARYENS_HOUSE_ID), getString(R.string.tab_targaryens));
        mViewPager.setAdapter(adapter);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    private void setupDrawer() {
        setRoundedAvatar();
        mNavigationView.setCheckedItem(R.id.stark_menu);
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.stark_menu:
                        mPresenter.showStarkHouse();
                        break;
                    case R.id.lannister_menu:
                        mPresenter.showLannisterHouse();
                        break;
                    case R.id.targaryen_menu:
                        mPresenter.showTangaryenHouse();
                        break;
                }
                return true;
            }
        });
    }

    private void setRoundedAvatar() {
        View headerLayout = mNavigationView.getHeaderView(0);
        CircleImageView circleImg = (CircleImageView) headerLayout.findViewById(R.id.circle_img);
        Picasso.with(this)
                .load(R.drawable.stark_icon)
                .fit()
                .centerCrop()
                .error(R.mipmap.ic_launcher)
                .placeholder(R.mipmap.ic_launcher)
                .into(circleImg);
    }

    @Override
    public void onItemClicked(long remoteId) {
        Intent intent = new Intent(CharacterListActivity.this, CharacterActivity.class);
        intent.putExtra(ConstantManager.EXTRA_CHARACTER_ID, remoteId);
        startActivity(intent);
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showMessage(int stringResourceId) {
        showMessage(getString(stringResourceId));
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage(getString(R.string.sorry_something_wrong));
            // TODO: 23.10.2016 send error stacktrace to crashlytics
        }
    }

    @Override
    public IActivityListPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void showStarkHouse() {
        mViewPager.setCurrentItem(0);
        mNavigationDrawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void showLannisterHouse() {
        mViewPager.setCurrentItem(1);
        mNavigationDrawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void showTangaryenHouse() {
        mViewPager.setCurrentItem(2);
        mNavigationDrawer.closeDrawer(GravityCompat.START);
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
