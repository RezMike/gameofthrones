package com.rezmike.gameofthrones.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.rezmike.gameofthrones.BuildConfig;
import com.rezmike.gameofthrones.R;
import com.rezmike.gameofthrones.data.managers.DataManager;
import com.rezmike.gameofthrones.data.storage.models.Alias;
import com.rezmike.gameofthrones.data.storage.models.Character;
import com.rezmike.gameofthrones.data.storage.models.CharacterDao;
import com.rezmike.gameofthrones.data.storage.models.Title;
import com.rezmike.gameofthrones.mvp.presenters.CharacterPresenter;
import com.rezmike.gameofthrones.mvp.presenters.ICharacterPresenter;
import com.rezmike.gameofthrones.mvp.views.ICharacterView;
import com.rezmike.gameofthrones.utils.ConstantManager;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CharacterActivity extends AppCompatActivity implements ICharacterView {

    CharacterPresenter mPresenter = CharacterPresenter.getInstance();

    private Character mCharacter;

    @BindView(R.id.coordinator_layout)
    CoordinatorLayout mCoordinatorLayout;

    //region ======================== Activity Cycle ========================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character);
        ButterKnife.bind(this);

        mPresenter.takeView(this);
        mPresenter.initView();

        setupToolbar();
    }

    @Override
    protected void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }

    //endregion

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        CharacterActivity.this.finish();
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showMessage(int stringResourceId) {
        showMessage(getString(stringResourceId));
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage(getString(R.string.sorry_something_wrong));
            // TODO: 23.10.2016 send error stacktrace to crashlytics
        }
    }

    @Override
    public ICharacterPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void initCharacter() {
        long characterId = getIntent().getLongExtra(ConstantManager.EXTRA_CHARACTER_ID, 0);
        mCharacter = mPresenter.getCharacter(characterId);
        if (mCharacter == null) {
            showMessage(R.string.sorry_something_wrong);
        } else {
            initCharacterValues();
        }
    }

    private void initCharacterValues() {
        CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(mCharacter.getName());
        ImageView houseImage = (ImageView) findViewById(R.id.house_img);
        TextView words = (TextView) findViewById(R.id.info_words);
        TextView born = (TextView) findViewById(R.id.info_born);
        TextView died = (TextView) findViewById(R.id.info_died);
        TextView titles = (TextView) findViewById(R.id.info_titles);
        TextView aliases = (TextView) findViewById(R.id.info_aliases);
        Button fatherBtn = (Button) findViewById(R.id.info_father_btn);
        Button motherBtn = (Button) findViewById(R.id.info_mother_btn);

        String wordsString = mCharacter.getWords();
        switch (wordsString) {
            case ConstantManager.STARKS_WORDS:
                houseImage.setImageResource(R.drawable.stark_bg);
                break;
            case ConstantManager.LANNISTERS_WORDS:
                houseImage.setImageResource(R.drawable.lannister_bg);
                break;
            case ConstantManager.TANGARYENS_WORDS:
                houseImage.setImageResource(R.drawable.targaryen_bg);
                break;
            default:
                houseImage.setImageResource(R.drawable.splash_screen);
                break;
        }

        if (!mCharacter.getDied().isEmpty()) {
            died.setText(mCharacter.getDied());
            String series = mCharacter.getLastSeries();
            if (!series.isEmpty()) {
                showMessage("Этот персонаж умер в " + series.substring(series.length() - 1) + " сезоне");
            }
        } else {
            died.setText("-");
        }
        words.setText(mCharacter.getWords().isEmpty() ? "-" : mCharacter.getWords());
        born.setText(mCharacter.getBorn().isEmpty() ? "-" : mCharacter.getBorn());
        titles.setText(getStringFromTitles(mCharacter.getTitles()));
        aliases.setText(getStringFromAliases(mCharacter.getAliases()));
        if (mCharacter.getFather().isEmpty()) {
            fatherBtn.setText("NO");
        } else {
            String fatherName = DataManager.getInstance().getDaoSession().queryBuilder(Character.class)
                    .where(CharacterDao.Properties.RemoteId.eq(mCharacter.getFather())).build().unique().getName();
            fatherBtn.setText(fatherName);
            fatherBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(CharacterActivity.this, CharacterActivity.class);
                    intent.putExtra(ConstantManager.EXTRA_CHARACTER_ID, Long.parseLong(mCharacter.getFather()));
                    startActivity(intent);
                }
            });
        }
        if (mCharacter.getMother().isEmpty()) {
            motherBtn.setText("NO");
        } else {
            String motherName = DataManager.getInstance().getDaoSession().queryBuilder(Character.class)
                    .where(CharacterDao.Properties.RemoteId.eq(mCharacter.getMother())).build().unique().getName();
            motherBtn.setText(motherName);
            motherBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(CharacterActivity.this, CharacterActivity.class);
                    intent.putExtra(ConstantManager.EXTRA_CHARACTER_ID, Long.parseLong(mCharacter.getMother()));
                    startActivity(intent);
                }
            });
        }
    }

    private String getStringFromTitles(List<Title> list) {
        if (list.isEmpty()) return "-";
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < list.size() - 1; ++i) {
            builder.append(list.get(i).getTitle());
            builder.append('\n');
        }
        builder.append(list.get(list.size() - 1).getTitle());
        return builder.toString();
    }

    private String getStringFromAliases(List<Alias> list) {
        if (list.isEmpty()) return "-";
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < list.size() - 1; ++i) {
            builder.append(list.get(i).getAlias());
            builder.append('\n');
        }
        builder.append(list.get(list.size() - 1).getAlias());
        return builder.toString();
    }
}
