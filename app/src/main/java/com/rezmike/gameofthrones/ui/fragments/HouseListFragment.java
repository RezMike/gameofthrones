package com.rezmike.gameofthrones.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.rezmike.gameofthrones.R;
import com.rezmike.gameofthrones.data.storage.models.Character;
import com.rezmike.gameofthrones.mvp.presenters.HouseListPresenter;
import com.rezmike.gameofthrones.mvp.presenters.IHouseListPresenter;
import com.rezmike.gameofthrones.mvp.views.IHouseListView;
import com.rezmike.gameofthrones.utils.ConstantManager;

import java.util.List;

public class HouseListFragment extends ListFragment implements IHouseListView {

    HouseListPresenter mPresenter = HouseListPresenter.getInstance();

    private List<Character> mCharacters;
    private HouseListCallbacks mCallbacks;

    public interface HouseListCallbacks {
        void onItemClicked(long remoteId);
        void showMessage(String message);
        void showMessage(int stringResourceId);
        void showError(Throwable e);
    }

    public static HouseListFragment newInstance(int houseId) {
        Bundle args = new Bundle();
        args.putInt(ConstantManager.HOUSE_ID, houseId);
        HouseListFragment fragment = new HouseListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    //region ======================== Fragment cycle ========================

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallbacks = (HouseListCallbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter.takeView(this);
        mPresenter.initView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_house_list, container, false);
        setListAdapter(new HouseListAdapter(mCharacters));
        return view;
    }

    @Override
    public void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }

    //endregion

    @Override
    public void showMessage(String message) {
        mCallbacks.showMessage(message);
    }

    @Override
    public void showMessage(int stringResourceId) {
        mCallbacks.showMessage(stringResourceId);
    }

    @Override
    public void showError(Throwable e) {
        mCallbacks.showError(e);
    }

    @Override
    public IHouseListPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void initializeCharacters() {
        switch (getArguments().getInt(ConstantManager.HOUSE_ID)) {
            case ConstantManager.STARKS_HOUSE_ID:
                mCharacters = mPresenter.getStarkList();
                break;
            case ConstantManager.LANNISTERS_HOUSE_ID:
                mCharacters = mPresenter.getLannisterList();
                break;
            case ConstantManager.TARGARYENS_HOUSE_ID:
                mCharacters = mPresenter.getTangaryenList();
                break;
        }
    }

    @Override
    public void onItemClick(long remoteId) {
        mCallbacks.onItemClicked(remoteId);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        mPresenter.onItemClick(Long.parseLong(mCharacters.get(position).getRemoteId()));
    }

    private class HouseListAdapter extends ArrayAdapter {

        public HouseListAdapter(List<Character> members) {
            super(getActivity(), 0, members);
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(R.layout.item_member_list, null);
            }

            Character character = mCharacters.get(position);

            ImageView arms = (ImageView) convertView.findViewById(R.id.member_arms);
            switch (getArguments().getInt(ConstantManager.HOUSE_ID)) {
                case ConstantManager.STARKS_HOUSE_ID:
                    arms.setImageResource(R.drawable.stark_icon);
                    break;
                case ConstantManager.LANNISTERS_HOUSE_ID:
                    arms.setImageResource(R.drawable.lannister_icon);
                    break;
                case ConstantManager.TARGARYENS_HOUSE_ID:
                    arms.setImageResource(R.drawable.targaryen_icon);
                    break;
            }

            TextView name = (TextView) convertView.findViewById(R.id.member_name);
            name.setText(character.getName());

            TextView nickname = (TextView) convertView.findViewById(R.id.member_nickname);
            if (!character.getTitles().isEmpty()) {
                nickname.setText(character.getTitles().get(0).getTitle());
            } else if (!character.getAliases().isEmpty()) {
                nickname.setText(character.getAliases().get(0).getAlias());
            } else nickname.setText("");

            return convertView;
        }
    }
}